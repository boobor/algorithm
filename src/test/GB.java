package test;

import com.bobo.data.DataStore;

public class GB {

    public static void main(String[] args) {
        int data[]= DataStore.data;
        px(data);
    }

    private static void px(int[] data) {
        gbpx(data,0,data.length-1);
    }

    private static void gbpx(int[] data, int left, int right) {
        if(left==right){
            return;
        }
        int mid=left+(right-left)/2;
        gbpx(data,left,mid);
        gbpx(data,mid+1,right);
        marge(data,left,mid,right);

    }

    private static void marge(int[] data, int left, int mid, int right) {
        int i=0;
        int l=left;
        int r=mid+1;
        int temp[]=new int [right-left+1];
        while (l<=left&&r<=right){
            temp[i++]=data[l]>data[r]?data[r++]:data[l++];
        }
        while (l<=left){
            temp[i++]=data[l++];
        }
        while (r<=right){
            temp[i++]=data[r++];
        }
        for (int j = 0; j <temp.length ; j++) {
            data[l+j]=data[j];
        }
    }
}
