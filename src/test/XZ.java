package test;

import com.bobo.data.DataStore;
import com.bobo.util.Utils;

import java.util.Arrays;

public class XZ {
        public static void main(String[] args) {
        int data[]= DataStore.data;
        px(data);
        System.out.println("args = " + Arrays.toString(data));
    }

        private static void px(int[] data) {
            for (int i = 0; i <data.length ; i++) {
                int minidx=i;
                int temp=data[i];
                for (int j = i+1; j <data.length ; j++) {
                    if (data[j]<temp){
                        minidx=j;
                    }
                }
                data[i]=data[minidx];
                data[minidx]=temp;
            }
        }
}
