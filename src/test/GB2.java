package test;

import com.bobo.data.DataStore;

import java.util.Arrays;

public class GB2 {

    public static void main(String[] args) {
        int [] data = DataStore.data;
        px(data);
        System.out.println("args = " + Arrays.toString(data));
    }

    private static void px(int [] data) {
        px(data,0,data.length-1);
    }

    private static void px(int[] data, int l, int r) {
        if (l==r){
            return;
        }
        int mid =l+(r-l)/2;
        px(data,l,mid);
        px(data,mid+1,r);
        marge(data,l,mid,r);
    }

    private static void marge(int[] data, int l, int mid, int r) {
        int lp=l;
        int rp=mid+1;
        int i=0;
        int temp[]=new int[r-l+1];
        while (lp<=mid&&rp<=r){
            temp[i++]=data[lp]>data[rp]?data[rp++]:data[lp++];
        }
        while (lp<=mid){
            temp[i++]=data[lp++];
        }
        while(rp<=r){
            temp[i++]=data[rp++];
        }
        for (int j = 0; j <temp.length; j++) {
            data[l+j]=temp[j];
        }
    }

}
