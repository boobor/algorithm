package test;

import com.bobo.data.DataStore;
import com.bobo.util.Utils;

import java.util.Arrays;

public class MP {

    public static void main(String[] args) {
        int data[]= DataStore.data;
        px(data);
        System.out.println("args = " + Arrays.toString(data));
    }

    private static void px(int[] data) {
        for (int i = 1; i <data.length ; i++) {
            for (int j = 0; j <data.length-i ; j++) {
                if(data[j]>data[j+1]){
                    Utils.swap(data,j,j+1);
                }
            }
        }
    }
}
