package test;

import com.bobo.data.DataStore;

import java.util.Arrays;

public class KS {

    public static void main(String[] args) {
        int data[]= DataStore.data;
        px(data);
        System.out.println("args = " + Arrays.toString(data));
    }

    private static void px(int[] data) {
        px(data,0,data.length-1);
    }

    private static void px(int[] data, int l, int r) {
        if (l>=r)
            return;
        int mid=part(data,l,r);
        px(data,l,mid);
        px(data,mid+1,r);
    }

    private static int part(int[] data, int l, int r) {
        int temp=data[l];
        int k=l;
        for (int i = l+1; i <=r ; i++) {
            if(data[i]<temp){
                k++;
                int tm=data[i];
                data[i]=data[k];
                data[k]=tm;
            }
        }
        data[l]=data[k];
        data[k]=temp;
        return k;
    }

}
