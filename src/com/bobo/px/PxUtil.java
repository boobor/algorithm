package com.bobo.px;

public class PxUtil implements PxImpl {
    private PxImpl px;

    public PxUtil(PxImpl px) {
        this.px = px;
    }

    @Override
    public void px(int[] data) {
        px.px(data);
    }
}
