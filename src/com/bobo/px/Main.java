package com.bobo.px;

import com.bobo.data.DataStore;
import com.bobo.px.impl.*;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] data= DataStore.data;
//        PxUtil util=new PxUtil(new MaoPao());
//        PxUtil util=new PxUtil(new XuanZhe());
//        PxUtil util=new PxUtil(new Insert());
//        PxUtil util=new PxUtil(new Shell());
//        PxUtil util=new PxUtil(new KaiSu());
//        PxUtil util=new PxUtil(new GuiBing());
        PxUtil util=new PxUtil(new HeapSimple());
        util.px(data);
        System.out.println("data = " + Arrays.toString(data));
    }
}
