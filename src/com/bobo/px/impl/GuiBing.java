package com.bobo.px.impl;

import com.bobo.px.PxImpl;

public class GuiBing implements PxImpl {
    @Override
    public void px(int[] data) {
        fd(data,0,data.length-1);
    }
    public void fd(int [] data,int left, int right ){
        if (left==right){
            return;
        }
        int mid=left+(right-left)/2;
        fd(data,left,mid);
        fd(data,mid+1,right);
        marge(data,left,mid,right);
    }

    private void marge(int[] data, int left,int mid, int right) {
        int [] temp=new int[right-left+1];
        int i=0;
        int ld=left;
        int rd=mid+1;
        while (ld<=mid&&rd<=right){
            temp[i++]=data[ld]>data[rd]?data[rd++]:data[ld++];
        }
        while (ld<=mid){
            temp[i++]=data[ld++];
        }
        while (rd<=right){
            temp[i++]=data[rd++];
        }
        for (int j = 0; j <temp.length ; j++) {
            data[left+j]=temp[j];
        }
    }


}
