package com.bobo.px.impl;

import com.bobo.px.PxImpl;
import com.bobo.util.Utils;

import javax.rmi.CORBA.Util;

public class KaiSu implements PxImpl {
    @Override
    public void px(int[] data) {
        ks(data,0,data.length-1);
    }

    private void ks(int data[] , int left, int right ){
        if (left<right){
            int md=parts(data,left,right);
            ks(data,left,md);
            ks(data,md+1,right);
        }
    }

    private int parts(int[] data, int left, int right) {
        int temp=data[left];
        int k=left;
        for (int i = left+1; i <=right ; i++) {
            if (data[i]<temp){
                k++;
                Utils.swap(data,k,i);
            }
        }
        Utils.swap(data,left,k);
        return k;
    }
}
