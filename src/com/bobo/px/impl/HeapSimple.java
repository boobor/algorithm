package com.bobo.px.impl;

import com.bobo.px.PxImpl;

public class HeapSimple implements PxImpl {

    private  void heapAjust(int data[],int leng,int i){
        int k=i;
        int temp =data[k];
        int index =2*k+1;
        while(index<leng){
            if(index+1<leng){
                if(data[index+1]>data[index]){
                    index=index+1;
                }
            }
                if(data[index]>temp){
                    data[k]=data[index];
                    k=index;
                    index=2*k+1;
                }else{
                    break;
                }

        }
        data[k]=temp;
    }


    @Override
    public void px(int[] data) {
        for (int i = data.length/2-1; i >=0 ; i--) {
            heapAjust(data,data.length,i);
        }
        for (int i = data.length-1; i >=0 ; i--) {
            int temp=data[i];
            data[i]=data[0];
            data[0]=temp;
            heapAjust(data,i,0);
        }
    }
}
