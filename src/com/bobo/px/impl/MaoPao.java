package com.bobo.px.impl;

import com.bobo.px.PxImpl;
import com.bobo.util.Utils;

public class MaoPao implements PxImpl {
    @Override
    public void px(int[] data) {
        for (int i = 1; i <data.length ; i++) {
            for (int j = 0; j <data.length-i ; j++) {
                if(data[j+1]<data[j]){
                    Utils.swap(data,j+1,j);
                }
            }
        }
    }
}
