package com.bobo.px.impl;

import com.bobo.px.PxImpl;
import com.bobo.util.Utils;

public class Insert implements PxImpl {
    @Override
    public void px(int[] data) {
        int leng=data.length;
        for (int i = 1; i <leng ; i++) {
            int temp=data[i];
            int k=i-1;
            while (k>=0&&data[k]>temp){
                Utils.swap(data,k+1,k);
                k--;
            }
            data[k+1]=temp;
        }

    }
}
