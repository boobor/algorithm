package com.bobo.px.impl;

import com.bobo.px.PxImpl;
import com.bobo.util.Utils;

import javax.rmi.CORBA.Util;

public class Shell implements PxImpl {
    @Override
    public void px(int[] data) {
        int leng=data.length;
        int grap =leng;
        while((grap=grap/2)>0){
            for (int i = grap; i <leng ; i++) {
                int temp=data[i];
                int k=i-grap;
                while(k>=0&&data[k]>temp){
                    Utils.swap(data,k,k+grap);
                    k=k-grap;
                }
                data[k+grap]=temp;
            }
        }
    }
}
